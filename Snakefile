from csv import DictReader, DictWriter

with open("from-paper/genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]

with open("SraRunTable.txt") as f_in:
    reader = DictReader(f_in)
    SRA = list(reader)

TARGET_SRA_FASTQ = expand("from-sra/{srr}_{rp}.fastq.gz", srr=[row["Run"] for row in SRA], rp=[1, 2])
TARGET_GBF = expand("from-genbank/{acc}.gbf", acc=GB_ACCESSIONS)
TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)
TARGET_SHEETS = expand("output/{sheet}.csv", sheet=["seqs"])

rule all:
    input: TARGET_SHEETS + TARGET_GBF + TARGET_GBF_FASTA + TARGET_SRA_FASTQ

rule all_gbf:
    input: TARGET_GBF

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule make_seqs_sheet:
    output: "output/seqs.csv"
    input: TARGET_GBF_FASTA
    shell: "python scripts/make_seqs_sheet.py {input} > {output}"

rule download_gbf:
    """Download one GBF text file per GenBank accession."""
    output: "from-genbank/{acc}.gbf"
    shell: "python scripts/download_genbank.py {wildcards.acc} gb > {output}"

rule download_gbf_fa:
    """Download one FASTA file per GenBank accession."""
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_genbank.py {wildcards.acc} fasta > {output}"

rule all_sra_fastq:
    input: TARGET_SRA_FASTQ

rule sra_fastq:
    output: expand("from-sra/{{srr}}_{rp}.fastq.gz", rp=[1, 2])
    shell: "fastq-dump --split-files --gzip {wildcards.srr} --outdir from-sra"
