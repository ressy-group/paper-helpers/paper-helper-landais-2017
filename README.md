# Data Gathering from Landais 2017

Some scripts to aggregate results and metadata from:

HIV Envelope Glycoform Heterogeneity and Localized Diversity Govern the Initiation and Maturation of a V2 Apex Broadly Neutralizing Antibody Lineage
Elise Landais et al.
Immunity, Volume 47, 990 – 1003
<https://doi.org/10.1016/j.immuni.2017.11.002>

## Quick Reference

 * Subject: PC064 (AKA PC64)
 * Antibody Lineage: PCT64
 * mAb and inferred sequence NCBI GenBank accessions: MF565855 - MF565933
 * Antibody repertoire NGS NCBI BioProject accession: PRJNA396773 (which
   includes SRP114571 as listed in one part of the text, not 114471 elsewhere
   which I think is a typo)

IMGT germline assignments:

    IGHV3-15*01
    IGHD3-3*01
    IGHJ6*03
    IGKV3-20*01
    IGKJ3*01

Misc notes:

 * UCA CDRH3 in Fig S2A does not match the "infGL" sequence deposited at
   MF565855

See also:

 * [paper-helper-macleod-2016](https://gitlab.com/ressy-group/paper-helpers/paper-helper-macleod-2016)
   for PCDN lineage from [10.1016/j.immuni.2016.04.016](https://doi.org/10.1016/j.immuni.2016.04.016)
 * [Briney 2016 (10.1038/srep23901)](https://doi.org/10.1038/srep23901) for Clonify/AbAnalysis
 * <https://github.com/briney/abstar>
