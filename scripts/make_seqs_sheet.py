#!/usr/bin/env python

"""
Make CSV of repertoire-derived seq attributes from downloaded GenBank files.
"""

import re
import sys
from csv import DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    match = re.match(
        "([A-Z0-9.]+) Homo sapiens isolate ([^ ]+) anti-HIV immunoglobulin "
        "(heavy|light) chain variable region mRNA, partial cds", txt)
    return match.groups()

FIELDS = [
    "Entry",
    "Category",
    "SeqType",
    "Timepoint",
    "Chain",
    "Accession",
    "SeqID",
    "Seq"]

def make_seqs_sheet(fastas):
    all_attrs = []
    for fasta in fastas:
        with open(fasta) as f_in:
            for record in SeqIO.parse(f_in, "fasta"):
                fields = parse_seq_desc(record.description)
                entry = re.sub("-[HKL]C$", "", fields[1])
                attrs = {field: "" for field in FIELDS}
                attrs["Entry"] = entry
                attrs["Accession"] = fields[0]
                attrs["SeqID"] = fields[1]
                attrs["Chain"] = fields[2]
                attrs["Seq"] = str(record.seq)
                attrs["SeqType"] = "Observed"
                attrs["Category"] = "mab"
                attrs["Timepoint"] = ""
                if "infGL" in entry:
                    attrs["SeqType"] = "Inferred"
                    attrs["Category"] = "Inferred"
                elif "LMCA" in entry:
                    # "At 10 mpi, we identified a PCT64 HC sequence with very
                    # high germline sequence identity (99.4%), encoded by a
                    # fully germline V gene (VH3-15∗01) and a J gene (JH6∗03)
                    # containing only two nucleotide mutations (Figure S2A).
                    # This sequence was selected as the least mutated common
                    # ancestor for the HC (VH_LMCA)."
                    # The GenBank entry for this one claims Sanger, but, from
                    # the rest of the paper it looks to me like this must have
                    # been from the MiSeq dataset where the repertoire
                    # sequencing was done.  See also Figure S2, where it says
                    # the mAbs are shown in black (versus the NGS seqs)
                    attrs["Timepoint"] = "10"
                    attrs["Category"] = "NGS"
                elif re.match(".*[0-9]+[A-Z]", fields[1]):
                    attrs["Timepoint"] = re.sub("[^-]*-([0-9]+)[A-Z]", r"\1", entry)
                else:
                    raise ValueError
                all_attrs.append(attrs)
    def sorter(row):
        entry = row["Entry"]
        is_light = row["Chain"] == "light"
        is_mab = row["Category"] == "mab"
        tp = int(row["Timepoint"]) if row["Timepoint"] else 0
        return (is_light, is_mab, tp, entry)
    all_attrs = sorted(all_attrs, key=sorter)
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(all_attrs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
